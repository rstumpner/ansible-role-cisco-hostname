
[![pipeline status](https://gitlab.com/rstumpner/cisco-hostname/badges/master/pipeline.svg)](https://gitlab.com/rstumpner/cisco-hostname/commits/master)

# Ansible Role Cisco Hostname

This is a Ansible Role for setting Hostnames on Cisco Devices.

Aufbau:
- Build Cisco Config
  - All Availiable Build Modules
    - All ansible modes
    - tags: build
  - Cisco IOS CLI
    - All ansible modes
    - tags: 
      - build
      - build-cli
      - cli
- Testing Device Config
  - Snapshot of Device Running Config and write to Startup
    - All ansible modes
    - tags:
      - bak
      - test
      - state
  - Snapshot of Device state Config
    - All ansible modes
    - tags:
      - bak
      - test
      - state
  - Review Config changes (--diff)
    - All ansible modes
    - tags:
      - diff
      - test
  - Testing Config changes
    - All Ansible modes
    - tags:
      - test
- Deploy Device Config
  - Deploy Config with CLI
    - not ansible check-mode
    - tags:
      - deploy
      - deploy-oam-cli
      - deploy-cli-oam

Requirements:
    None

Role Variables:
    - hostname

Using this Role:
Drive to Ansible Role Directory:
    - git clone https://gitlab.com/rstumpner/cisco-hostname.git

Aktivate Role in a Playbook:

Example:
```YAML
- hosts: all
  roles:
     - cisco-hostname
```

Tested:
 - Cisco IOS
 - Cisco IOS-XE
 
License:
    MIT / BSD

Author Information:
roland@stumpner.at